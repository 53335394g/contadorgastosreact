import React, { useState } from "react";
import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";
const NewExpense = (props) => {
  const [visibleForm, setVisibleForm] = useState(false);
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.expenseDataRow(expenseData);
    goToNotVisibleForm();
  };
  const goToVisibleForm = () => {
    setVisibleForm(true);
  };
  const goToNotVisibleForm = () => {
    setVisibleForm(false);
  };
  return (
    <div className="new-expense">
      {!visibleForm && (
        <button onClick={goToVisibleForm}>Add New Expense.</button>
      )}
      {visibleForm && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          goToNotVisibleForm={goToNotVisibleForm}
        />
      )}
    </div>
  );
};

export default NewExpense;
