import React from "react";
import ChartBar from "./CharBar";
import "./Chart.css";
const Chart = (props) => {
  const dataPointsValues = props.dataPoints.map(x => x.value);
  const totalMaximum = Math.max(...dataPointsValues);
  return (
    <div className="chart">
      {props.dataPoints.map((x) => (
        <ChartBar
          key={x.label}
          value={x.value}
          maxValue={totalMaximum}
          label={x.label}
        />
      ))}
    </div>
  );
};

export default Chart;
