import React, { useState } from "react";
import Card from "../ui/Card";
import ExpensesList from "./ExpensesList";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesChart from "./ExpensesChart";
import "./Expenses.css";
const Expenses = (props) => {
  const [yearSelected, setYearSelected] = useState("2021");
  const filteredExpenses = props.expenses.filter((x) => {
    return x.date.getFullYear().toString() === yearSelected;
  });
  const filterChange = (selectedFilterYear) => {
    setYearSelected(selectedFilterYear);
  };

  return (
    <Card className="expenses">
      <ExpensesFilter selectedYear={yearSelected} filterChange={filterChange} />
      <ExpensesChart expenses={filteredExpenses} />
      <ExpensesList expenses={filteredExpenses} />
    </Card>
  );
};

export default Expenses;
